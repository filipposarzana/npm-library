# Modules
- Json Storage
- Downloader
- Paths

# Json Storage

```
#!javascript
jsonStorage = {
  set: function(id, value) {},
  get: function(id) {},
  remove: function(id) {},
  list: function(id) {}
}
```

Return **promises**

```
#!javascript
jsonStorage.set('test', {foo:'bar'}).then(function(response){
    console.log('foo');
}, function(error) {
    console.log('bar');
});
```

# Paths
Paths are **constants** useful all over the application

```
#!javascript
{
  assets: './_assets',
  storage: './_storage'
}
```

# Downloader

```
#!javascript
var downloader = new Downloader([{
  url:'http://ip.jsontest.com/',
  name:'ip.json'
}], folder || './_assets');
```

```
#!javascript
downloader.onComplete(function(response){
  console.log('complete');
});
```

```
#!javascript
downloader.download();
```

```
#!javascript
downloader.erase();
```

Downloader save files in **./_assets** folder and **erase()** erase all directory content
