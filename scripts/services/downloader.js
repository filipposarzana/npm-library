var paths = require('../constants/paths'),
  Download = require('download'),
  fs = require('fs-extra'),
  _ = require('lodash');
var Downloader = function(list, folder) {
  this.urls = list;
  this.counter = list.length;
  this.progressCounter = 0;
  this.folder = folder || paths.assets;
};
Downloader.prototype.onProgress = function(callback) {
  this.progressCallback = callback;
};
Downloader.prototype.onError = function(callback) {
  this.errorCallback = callback;
};
Downloader.prototype.onComplete = function(callback) {
  this.completeCb = callback;
};
Downloader.prototype.onEmptied = function(callback) {
  this.emptiedCallback = callback;
};
Downloader.prototype.onFileAnalyzed = function(callback) {
  this.fileAnalyzedCallback = callback;
};
Downloader.prototype.download = function() {
  var _self = this;
  var download = new Download({
    mode: '755'
  }).dest(_self.folder);
  _.forEach(_self.urls, function(uri) {
    download.get(uri.url).rename(uri.name).run(function(err, files) {
      _self.handleDownload(err, files);
    });
  });
};
Downloader.prototype.handleDownload = function(err, files) {
  if (err) {
    if (_.isFunction(this.errorCallback)) {
      this.errorCallback(err, files);
    }
    if (_.isFunction(this.fileAnalyzedCallback)) {
      this.fileAnalyzedCallback(err, files);
    }
  } else {
    this.progressCounter++;
    if (_.isFunction(this.progressCallback)) {
      this.callback(parseInt(this.progressCounter / this.counter * 100));
    }
    if (this.progressCounter === this.counter && _.isFunction(this.completeCb)) {
      this.completeCb();
    }
  }
};
Downloader.prototype.erase = function() {
  var _self = this;
  fs.emptyDir(_self.folder, function(err) {
    if (!err && _.isFunction(_self.emptiedCallback)) {
      _self.emptiedCallback();
    }
  });
};
module.exports = Downloader;
