var paths = require('../constants/paths'),
  store = require('json-fs-store')(paths.storage),
  Q = require('q'),
  _ = require('lodash');
var jsonStorage = {
  set: function(id, value) {
    var deferred = Q.defer();
    store.add({
      id: id,
      data: value
    }, function(err) {
      if (err) {
        deferred.reject(err);
      } else {
        deferred.resolve(value);
      }
    });
    return deferred.promise;
  },
  get: function(id) {
    var deferred = Q.defer();
    store.load(id, function(err, value) {
      if (err || _.isUndefined(value.data)) {
        deferred.reject(err);
      } else {
        deferred.resolve(value.data);
      }
    });
    return deferred.promise;
  },
  remove: function(id) {
    var deferred = Q.defer();
    store.remove(id, function(err) {
      if (err) {
        deferred.reject(err);
      } else {
        deferred.resolve(id);
      }
    });
    return deferred.promise;
  },
  list: function(id) {
    var deferred = Q.defer();
    store.list(function(err, value) {
      if (err) {
        deferred.reject(err);
      } else {
        deferred.resolve(value);
      }
    });
    return deferred.promise;
  }
};
module.exports = jsonStorage;
