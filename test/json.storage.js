var jsonStorage = require('../scripts/services/json.storage'),
  _ = require('lodash'),
  object = {
    foo: 'bar'
  };
describe('jsonStorage', function() {
  describe('#set()', function() {
    before(function() {
      jsonStorage.list().then(function(response) {
        _.forEach(function(item) {
          jsonStorage.remove(item.id);
        });
      });
    });
    it('should set in storage without errors', function(done) {
      jsonStorage.set('test', object).then(function() {
        done();
      }, function(err) {
        done(err);
      });
    });
  });
  describe('#get()', function() {
    it('should return object equals to what has been set', function(done) {
      jsonStorage.get('test').then(function(response) {
        if (_.isEqual(response, object)) {
          done();
        } else {
          done('Object are not identical');
        }
      }, function(err) {
        done(err);
      });
    });
  });
  describe('#list()', function() {
    it('should return array with length 1', function(done) {
      jsonStorage.list().then(function(response) {
        if (response.length === 1) {
          done();
        } else {
          done('Length of storage array list is not 1');
        }
      }, function(err) {
        done(err);
      });
    });
  });
  describe('#remove()', function() {
    it('should remove without errors', function(done) {
      jsonStorage.remove('test').then(function(response) {
        if (response === 'test') {
          done();
        } else {
          done('Removed ID is not "test"');
        }
      }, function(err) {
        done(err);
      });
    });
  });
  describe('#list()', function() {
    it('should return array with length 0', function(done) {
      jsonStorage.list().then(function(response) {
        if (response.length === 0) {
          done();
        } else {
          done('Length of storage array list is not 0');
        }
      }, function(err) {
        done(err);
      });
    });
  });
});
