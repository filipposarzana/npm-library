var Downloader = require('../scripts/services/downloader'),
  paths = require('../scripts/constants/paths'),
  _ = require('lodash'),
  fs = require('fs'),
  downloader;
beforeEach(function() {
  downloader = new Downloader([{
    url: 'http://ip.jsontest.com/',
    name: 'ip.json'
  }]);
});
describe('Downloader', function() {
  describe('#init()', function() {
    it('should have list length equals to 1', function(done) {
      if (!_.isUndefined(downloader.urls) && downloader.urls.length === 1) {
        done();
      } else {
        done('Downloader is not defined or list is different than 1');
      }
    });
  });
  describe('#download()', function() {
    it('should save files in _assets folder', function(done) {
      downloader.onComplete(function(response) {
        if (fs.existsSync(paths.assets + '/ip.json')) {
          done();
        } else {
          done('ip.json does not exists in directory _assets');
        }
      });
      downloader.download();
    });
  });
  describe('#erase()', function() {
    it('should erase files in _assets folder', function(done) {
      downloader.onEmptied(function(response) {
        if (!fs.existsSync(paths.assets + '/ip.json')) {
          done();
        } else {
          done('ip.json still exists in directory _assets');
        }
      });
      downloader.erase();
    });
  });
});
